package com.opsgenie.workshop;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.opsgenie.workshop.accessor.DynamoDBAccessor;
import com.opsgenie.workshop.dao.TeamDaoDynamoDBImpl;
import com.opsgenie.workshop.model.Team;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import java.util.HashMap;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TeamRepositoryDynamoDBTest {
    @Mock
    private DynamoDBAccessor accessor;

    @Mock
    private DynamoDBMapper mapper;

    private TeamDaoDynamoDBImpl teamDao;

    @Before
    public void init() {
        this.teamDao = new TeamDaoDynamoDBImpl();
        this.teamDao.setAccessor(accessor);
       when(accessor.getMapper()).thenReturn(mapper);

    }

    @Test
    public void shouldGetTeam() throws Exception {
        Team expectedTeam = new Team("team1", "heimdall", new HashMap<>());
        when(mapper.load(Team.class, "team1")).thenReturn(expectedTeam);
        assertThat(teamDao.getTeam("team1"), is(equalTo(expectedTeam)));
    }
    @Test
    public void shouldAddTeam() throws Exception {
        Team expectedTeam = new Team("team1", "heimdall", new HashMap<>());
        teamDao.addTeam(expectedTeam);
        verify(mapper, times(1)).save(expectedTeam);
    }
    @Test
    public void shouldDeleteTeam() throws Exception {
        Team expectedTeam = new Team("team5", "heimdall", new HashMap<>());
        when(teamDao.getTeam(expectedTeam.getId())).thenReturn(expectedTeam);
        teamDao.deleteTeam(expectedTeam.getId());
        verify(mapper, times(1)).delete(expectedTeam);
    }

}
