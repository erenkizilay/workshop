package com.opsgenie.workshop;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.opsgenie.workshop.controller.TeamController;
import com.opsgenie.workshop.model.Team;
import com.opsgenie.workshop.model.User;
import com.opsgenie.workshop.service.TeamService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(TeamController.class)
public class TeamControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private TeamService teamService;

    public String jsonify(Object o) throws JsonProcessingException {
        com.fasterxml.jackson.databind.ObjectWriter objectWriter = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String jsonOutput = objectWriter.writeValueAsString(o);
        return jsonOutput;
    }

    @Test
    public void shouldGetTeams() throws Exception {
        HashMap<String, Team> expectedTeams = new HashMap<>();
        expectedTeams.put("team1", new Team("team1", "heimdall", new HashMap<>()));
        when(teamService.getTeams()).thenReturn(expectedTeams);
        mockMvc.perform(get("/teams"))
                .andExpect(status().isOk())
                .andExpect(content().json(jsonify(expectedTeams)));
    }
    @Test
    public void shouldAddTeam() throws Exception{
        Team expectedTeam = new Team("team1", "heimdall", new HashMap<>());
        mockMvc.perform(post("/team/create").
                contentType(MediaType.APPLICATION_JSON).
                content(jsonify(expectedTeam))).
                andExpect(status().isOk());
    }
    @Test
    public void shouldUpdateTeam() throws Exception{
        Team expectedTeam = new Team("team1", "heimdall", new HashMap<>());
        mockMvc.perform(put("/team/update").
                contentType(MediaType.APPLICATION_JSON).
                content(jsonify(expectedTeam))).
                andExpect(status().isOk());
    }
    @Test
    public void shouldDeleteTeam() throws Exception{
        Team expectedTeam = new Team("team1", "heimdall", new HashMap<>());
        mockMvc.perform(delete("/team/delete/team1")).
                andExpect(status().isOk());
    }
    @Test
    public void shouldAssignUserToTeam() throws Exception{
        Team expectedTeam = new Team("team1", "heimdall", new HashMap<>());
        User user = new User("1","Eren");
        mockMvc.perform(post("/team/assign/team1").
                contentType(MediaType.APPLICATION_JSON).
                content(jsonify(user))).
                andExpect(status().isOk());
    }
    @Test
    public void shouldTryToAssign() throws  Exception{
        List<String> userIds =  new ArrayList<>();
        userIds.add("1");
        userIds.add("2");
        mockMvc.perform(post("/team/team5/assignTeamMember")
        .contentType(MediaType.APPLICATION_JSON)
        .content(jsonify(userIds)))
                .andExpect(status().isOk());

    }

}
