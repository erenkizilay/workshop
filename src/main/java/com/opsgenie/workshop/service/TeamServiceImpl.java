package com.opsgenie.workshop.service;

import com.amazonaws.services.sqs.model.SendMessageBatchRequest;
import com.amazonaws.services.sqs.model.SendMessageBatchRequestEntry;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import com.opsgenie.workshop.dto.MessageConverter;
import com.opsgenie.workshop.dto.TeamMessage;
import com.opsgenie.workshop.sqs.SqsAccessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import com.opsgenie.workshop.dao.TeamDao;
import com.opsgenie.workshop.model.Team;
import com.opsgenie.workshop.model.User;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class TeamServiceImpl implements TeamService {

    private SqsAccessor sqsAccessor;
    private TeamDao teamDao;

    //this constructor created for test purposes
    @Autowired
    public TeamServiceImpl(@Qualifier("teamDaoDynamoDB")TeamDao teamDao, SqsAccessor sqsAccessor) {

        this.teamDao = teamDao;
        this.sqsAccessor = sqsAccessor;
    }

    @Override
    public Team getTeam(String id) {

        return this.teamDao.getTeam(id);
    }

    @Override
    public Map<String, Team> getTeams() {
        return teamDao.getTeams();
    }

    @Override
    public void addTeam(Team team) {
        this.teamDao.addTeam(team);
    }

    @Override
    public void addUserToTeam(String userId, String teamId) {

    }

    @Override
    public void updateTeam(Team team) {
        this.teamDao.updateTeam(team);
    }

    @Override
    public void deleteTeam(String id) {
        this.teamDao.deleteTeam(id);
    }

    @Override
    public void assignUserToTeam(User user, String teamId) {
        this.teamDao.assignUserToTeam(user, teamId);
    }

    @Override
    public void tryAssignUsersToTeam(String teamId, List<String> userIds) {
        List<TeamMessage> messages = new ArrayList<>();
        for(String userId : userIds){
            messages.add(new TeamMessage(teamId, userId));
        }
        for(TeamMessage message : messages){
            sqsAccessor.sendMessageToQueue(message);
        }


    }

}
