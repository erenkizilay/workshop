package com.opsgenie.workshop.dao;

import com.opsgenie.workshop.model.Team;
import com.opsgenie.workshop.model.User;

import java.util.HashMap;
import java.util.Map;

public interface TeamDao {
    Team getTeam(String id);
    Map<String, Team> getTeams();
    void addTeam(Team team);
    void updateTeam(Team team);
    void deleteTeam(String id);
    void assignUserToTeam(User user, String teamId);
}
