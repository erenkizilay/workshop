package com.opsgenie.workshop.sqs;

import com.amazonaws.services.sqs.model.Message;
import com.opsgenie.workshop.dto.MessageConverter;
import com.opsgenie.workshop.dto.TeamMessage;
import com.opsgenie.workshop.model.Team;
import com.opsgenie.workshop.model.User;
import com.opsgenie.workshop.service.TeamService;
import com.opsgenie.workshop.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class AssignTeamMemberQueueProcessor {
    @Autowired
    private SqsAccessor sqsAccessor;

    @Autowired
    private TeamService teamService;

    @Autowired
    private UserService userService;

    @Scheduled(fixedDelay = 1)
    public void poll(){
        List<Message> messageList = sqsAccessor.receiveMessages();
        process(messageList);
        sqsAccessor.deleteMessages(messageList);

    }
    public void process(List<Message> messages){
        if(messages.size()>0){
            for(Message message : messages){
                TeamMessage teamMessage = MessageConverter.jsonToObject(message.getBody());
                System.out.println(teamMessage);
                User user = userService.getUser(teamMessage.getUserId());
                if(user!=null){
                    teamService.assignUserToTeam(user,teamMessage.getTeamId());
                }
            }
        }
    }

}
