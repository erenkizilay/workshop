package com.opsgenie.workshop;

import com.opsgenie.workshop.dao.UserDao;
import com.opsgenie.workshop.model.User;
import com.opsgenie.workshop.service.UserServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {
    private UserServiceImpl userService;
    @Mock
    private UserDao userDao;

    @Before
    public void setUp(){
        userService = new UserServiceImpl(userDao);

    }

    @Test
    public void listUsers() throws Exception{
        HashMap<String, User> expectedUsers = new HashMap<>();
        expectedUsers.put("1",new User("1","Eren"));
        expectedUsers.put("2",new User("2","Cem"));
        when(userDao.getUsers()).thenReturn(expectedUsers);
        Map<String, User> users = userService.getUsers();
        assertThat(users, is(equalTo(expectedUsers)));
    }
    @Test
    public void getUser() throws Exception{
        User expectedUser = new User("1","Eren");
        when(userDao.getUser("1")).thenReturn(expectedUser);
        assertThat(userService.getUser("1"), is(equalTo(expectedUser)));
    }
    @Test
    public void updateUser() throws Exception{
        User expectedUser = new User("1","Eren");

        userService.updateUser(expectedUser);

        verify(userDao, times(1)).updateUser(expectedUser);
    }
    @Test
    public void deleteUser() throws Exception{
        User user = new User("1","Eren");

        userService.deleteUser("1");

        verify(userDao, times(1)).deleteUser("1");
    }
    @Test
    public void createUser() throws Exception{
        User user = new User("1","Eren");

        userService.createUser(user);

        verify(userDao, times(1)).createUser(user);
    }
}
