package com.opsgenie.workshop.dao;

import org.springframework.stereotype.Repository;
import com.opsgenie.workshop.model.Team;
import com.opsgenie.workshop.model.User;

import javax.annotation.PostConstruct;
import java.util.HashMap;

@Repository("teamDaoInMemory")
public class TeamDaoImpl implements TeamDao {
    private HashMap<String, Team> teams = new HashMap<>();
     @PostConstruct
     void postConstruct() {
         teams.put("team1",new Team("team1","heimdall",new HashMap<>()));
         teams.put("team2",new Team("team2","sre",new HashMap<>()));
         // TODO user uarat
         // Team yarat
         // user --> team
     }

    @Override
    public Team getTeam(String id) {
        return teams.get(id);
    }

    @Override
    public HashMap<String, Team> getTeams() {
        return teams;
    }

    @Override
    public void addTeam(Team team) {
        this.teams.put(team.getId(),team);
    }

    @Override
    public void updateTeam(Team team) {
        teams.put(team.getId(),team);
    }

    @Override
    public void deleteTeam(String id) {
        teams.remove(id);
    }

    @Override
    public void assignUserToTeam(User user, String teamId) {
        Team team = teams.get(teamId);
        team.getUsers().put(user.getId(),user);
    }
}
