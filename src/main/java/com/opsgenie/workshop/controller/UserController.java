package com.opsgenie.workshop.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.opsgenie.workshop.model.User;
import com.opsgenie.workshop.service.UserService;

import java.util.HashMap;
import java.util.Map;

@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/hello", method = RequestMethod.GET)
    public String hello() {
        return "asd";
    }

    @RequestMapping(value = "/user/add", method = RequestMethod.POST)
    public void createUser(@RequestBody User user) {
        this.userService.createUser(user);
    }

    @RequestMapping(value = "/user/{id}", method = RequestMethod.GET)
    public User getUser(@PathVariable String id) {
        return this.userService.getUser(id);
    }

    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public Map<String, User> getUser() {
        return this.userService.getUsers();
    }

    @RequestMapping(value = "/user/update", method = RequestMethod.PUT)
    public void updateUser(@RequestBody User user) {
        this.userService.createUser(user);


    }


    @RequestMapping(value = "/user/delete/{id}", method = RequestMethod.DELETE)
    public void deleteUser(@PathVariable String id) {
        this.userService.deleteUser(id);
    }


}
