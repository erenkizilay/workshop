package com.opsgenie.workshop.dao;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.opsgenie.workshop.accessor.DynamoDBAccessor;
import com.opsgenie.workshop.model.User;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository(value = "userDaoDynamoDB")
public class UserDaoDynamoDBImpl implements UserDao {


    @Autowired
    private DynamoDBAccessor accessor;

    public void initialize(){

    }

    @Override
    public User getUser(String id) {
        return accessor.getMapper().load(User.class, id);
    }

    @Override
    public void createUser(User user) {
        accessor.getMapper().save(user);
    }

    @Override
    public Map<String, User> getUsers() {
        //return mapper.scan(Team.class, new DynamoDBScanExpression());
        //return null;
        List<User> users = accessor.getMapper().scan(User.class, new DynamoDBScanExpression());
        Map<String, User> userMap = new HashMap<>();
        for(User u : users){
            userMap.put(u.getId(),u);
        }
        return userMap;
    }

    @Override
    public void updateUser(User user) {
        accessor.getMapper().save(user);
    }

    @Override
    public void deleteUser(String id) {
        accessor.getMapper().delete(getUser(id));
    }
}
