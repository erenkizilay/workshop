package com.opsgenie.workshop.service;

import com.opsgenie.workshop.model.Team;
import com.opsgenie.workshop.model.User;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface TeamService {
    Team getTeam(String id);
    Map<String, Team> getTeams();
    void addTeam(Team team);
    void addUserToTeam(String userId, String teamId);
    void updateTeam(Team team);
    void deleteTeam(String id);
    void assignUserToTeam(User user, String teamId);
    void tryAssignUsersToTeam(String teamId, List<String> userIds);

}
