package com.opsgenie.workshop.service;

import com.opsgenie.workshop.model.User;

import java.util.HashMap;
import java.util.Map;

public interface UserService {
    User getUser(String id);
    void createUser(User user);
    Map<String, User> getUsers();
    void updateUser(User user);
    void deleteUser(String id);
}
