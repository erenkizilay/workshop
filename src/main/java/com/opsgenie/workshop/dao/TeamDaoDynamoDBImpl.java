package com.opsgenie.workshop.dao;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.opsgenie.workshop.accessor.DynamoDBAccessor;
import com.opsgenie.workshop.model.Team;
import com.opsgenie.workshop.model.User;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository(value = "teamDaoDynamoDB")
public class TeamDaoDynamoDBImpl implements TeamDao {
    @Autowired
    private DynamoDBAccessor accessor;

    @Override
    public Team getTeam(String id) {

        return accessor.getMapper().load(Team.class, id);
    }

    @Override
    public Map<String, Team> getTeams() {
        List<Team> teams = accessor.getMapper().scan(Team.class, new DynamoDBScanExpression());
        Map<String, Team> teamMap = new HashMap<>();
        for(Team t : teams) {
            teamMap.put(t.getId(),t);
        }
        return teamMap;
    }


    @Override
    public void addTeam(Team team) {
        accessor.getMapper().save(team);
    }

    @Override
    public void updateTeam(Team team) {
        accessor.getMapper().save(team);
    }

    @Override
    public void deleteTeam(String id) {
        accessor.getMapper().delete(getTeam(id));
    }

    @Override
    public void assignUserToTeam(User user, String teamId) {
        Team team = getTeam(teamId);
        team.getUsers().put(user.getId(), user);
        accessor.getMapper().save(team);
    }

    public DynamoDBAccessor getAccessor() {
        return accessor;
    }

    public void setAccessor(DynamoDBAccessor accessor) {
        this.accessor = accessor;
    }
}
