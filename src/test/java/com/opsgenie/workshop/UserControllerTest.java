package com.opsgenie.workshop;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.opsgenie.workshop.controller.UserController;
import com.opsgenie.workshop.model.User;
import com.opsgenie.workshop.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;


import java.util.HashMap;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(UserController.class)

public class UserControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserService userService;

    @Test
    public void shouldReturnUsers() throws Exception{
        HashMap<String, User> expectedUsers = new HashMap<String, User>();
        expectedUsers.put("11",new User("1","Eren"));
        expectedUsers.put("2asadasdad",new User("2","Cem"));
        when(userService.getUsers()).thenReturn(expectedUsers);
        expectedUsers.put("2",new User("2","Cem"));
        mockMvc.perform(get("/users"))
                .andExpect(status().isOk())
                .andExpect(content().json(jsonify(expectedUsers)));

    }
    public String jsonify(Object o) throws JsonProcessingException {
        com.fasterxml.jackson.databind.ObjectWriter objectWriter = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String jsonOutput = objectWriter.writeValueAsString(o);
        return  jsonOutput;
    }
    @Test
    public void shouldGetUser() throws Exception{
        User expectedUser = new User("1","Eren");
        when(userService.getUser("1")).thenReturn(expectedUser);
        mockMvc.perform(get("/user/1"))
                .andExpect(status().isOk())
                .andExpect(content().json(jsonify(expectedUser)));

    }
    @Test
    public void shouldAddUser() throws Exception{
        User expectedUser = new User("1","Eren");
        mockMvc.perform(post("/user/add").
                contentType(MediaType.APPLICATION_JSON).
                content(jsonify(expectedUser))).
                andExpect(status().isOk());
    }
    @Test
    public void shouldUpdateUser() throws Exception{
        User expectedUser = new User("1","Eren");
        mockMvc.perform(put("/user/update").
                contentType(MediaType.APPLICATION_JSON).
                content(jsonify(expectedUser))).
                andExpect(status().isOk());
    }
    @Test
    public void shouldDeleteUser() throws Exception{
        User expectedUser = new User("1","Eren");
        mockMvc.perform(delete("/user/delete/1")).
                andExpect(status().isOk());
    }



}
