package com.opsgenie.workshop.dao;

import com.opsgenie.workshop.model.User;

import java.util.HashMap;
import java.util.Map;

public interface UserDao {
    User getUser(String id);
    void createUser(User user);
    Map<String, User> getUsers();
    void updateUser(User user);
    void deleteUser(String id);

}
