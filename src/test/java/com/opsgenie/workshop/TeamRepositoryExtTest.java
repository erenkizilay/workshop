package com.opsgenie.workshop;

import com.opsgenie.workshop.accessor.DynamoDBAccessor;
import com.opsgenie.workshop.dao.TeamDaoDynamoDBImpl;
import com.opsgenie.workshop.model.Team;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.HashMap;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
@RunWith(MockitoJUnitRunner.class)
public class TeamRepositoryExtTest {

    private TeamDaoDynamoDBImpl teamDao;

    private DynamoDBAccessor accessor;

    @Before
    public void init(){
        accessor = new DynamoDBAccessor();
        teamDao = new TeamDaoDynamoDBImpl();
        teamDao.setAccessor(accessor);
    }

    @Test
    public void shouldAddTeam(){
        Team team = new Team("test1","testedAddedTeam",new HashMap<>());
        teamDao.addTeam(team);
        Team fetchedTeam = teamDao.getTeam(team.getId());
        assertThat(team, is(equalTo(fetchedTeam)));
    }
}
