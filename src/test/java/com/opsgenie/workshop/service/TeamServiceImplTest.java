package com.opsgenie.workshop.service;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import com.opsgenie.workshop.dao.TeamDao;
import com.opsgenie.workshop.dto.TeamMessage;
import com.opsgenie.workshop.model.Team;
import com.opsgenie.workshop.model.User;
import com.opsgenie.workshop.service.TeamServiceImpl;
import com.opsgenie.workshop.sqs.SqsAccessor;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.util.*;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class TeamServiceImplTest {
    private TeamServiceImpl teamService;
    @Mock
    private TeamDao teamDao;

    @Mock
    private SqsAccessor sqsAccessor;

    @Before
    public void setUp(){
        teamService =  new TeamServiceImpl(teamDao, sqsAccessor);
        //sqsAccessor.setSqsQueueUrl("some-queue");
    }
    @Test
    public void shouldGetTeams() throws Exception{
        HashMap<String, Team> expectedTeams = new HashMap<>();
        expectedTeams.put("team1",new Team("team1", "heimdall", new HashMap<>()));
        when(teamDao.getTeams()).thenReturn(expectedTeams);
        Map<String, Team> teams = teamService.getTeams();
        assertThat(teams, is(equalTo(expectedTeams)));
    }
    @Test
    public void shouldGetTeam() throws Exception{
        Team expectedTeam = new Team("team1", "heimdall", new HashMap<>());
        when(teamDao.getTeam("team1")).thenReturn(expectedTeam);
        assertThat(expectedTeam, is(equalTo(teamService.getTeam("team1"))));
    }
    @Test
    public void shouldAddTeam() throws Exception{
        Team expectedTeam = new Team("team1", "heimdall", new HashMap<>());
        teamService.addTeam(expectedTeam);
        verify(teamDao, times(1)).addTeam(expectedTeam);
    }
    @Test
    public void shouldAssignUserToTeam() throws Exception{
        Team expectedTeam = new Team("team1", "heimdall", new HashMap<>());
        User user = new User("1","Eren");
        teamService.assignUserToTeam(user,"team1");
        verify(teamDao, times(1)).assignUserToTeam(user,"team1");
    }
    @Test
    public void shouldUpdateTeam() throws Exception{
        Team expectedTeam = new Team("team1", "heimdall", new HashMap<>());
        teamService.updateTeam(expectedTeam);
        verify(teamDao, times(1)).updateTeam(expectedTeam);
    }
    @Test
    public void shouldDeleteTeam() throws Exception{
        Team expectedTeam = new Team("team1", "heimdall", new HashMap<>());
        teamService.deleteTeam("team1");
        verify(teamDao, times(1)).deleteTeam("team1");
    }

    @Test
    public void shouldTryAssign_withEmptyUserIds() throws Exception{
        teamService.tryAssignUsersToTeam("team1",Collections.emptyList());
        verifyZeroInteractions(sqsAccessor);
    }


    @Test
    public void shouldTryAssign() throws Exception{
        String teamId = "teamId";
        List<String> userIds = Arrays.asList("user1", "user2", "user3");

        doThrow(new RuntimeException("something broken"))
                .when(sqsAccessor).sendMessageToQueue(new TeamMessage(teamId, "user2"));

        try {
            teamService.tryAssignUsersToTeam(teamId,userIds);
            fail("bu satira girmemeli");
        } catch (RuntimeException ignored) {
        }


        /*
        for (String userId : userIds){
            verify(sqsAccessor).sendMessageToQueue(new TeamMessage(teamId, userId));
        }
        */
        verify(sqsAccessor).sendMessageToQueue(new TeamMessage(teamId, userIds.get(0)));
        verify(sqsAccessor).sendMessageToQueue(new TeamMessage(teamId, userIds.get(1)));
        verifyNoMoreInteractions(sqsAccessor);
    }

}
