package com.opsgenie.workshop.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.opsgenie.workshop.model.Team;
import com.opsgenie.workshop.model.User;
import com.opsgenie.workshop.service.TeamService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class TeamController {

    @Autowired
    private TeamService teamService;

    @RequestMapping(value = "/teams", method = RequestMethod.GET)
    public Map<String, Team> getTeams() {
        return this.teamService.getTeams();
    }

    @RequestMapping(value = "/team/create", method = RequestMethod.POST)
    public void createTeam(@RequestBody Team team) {
        System.out.println(team.getName());
        teamService.addTeam(team);
    }

    @RequestMapping(value = "/team/update", method = RequestMethod.PUT)
    public void updateTeam(@RequestBody Team team) {
        teamService.updateTeam(team);
    }

    @RequestMapping(value = "/team/delete/{id}", method = RequestMethod.DELETE)
    public void deleteTeam(@PathVariable String id) {
        teamService.deleteTeam(id);
    }

    @RequestMapping(value = "/team/assign/{teamId}", method = RequestMethod.POST)
    public void assignUser(@PathVariable String teamId, @RequestBody User user) {
        teamService.assignUserToTeam(user, teamId);
    }
    @RequestMapping(value = "/team/{teamId}/assignTeamMember", method = RequestMethod.POST)
    public void assignUsersToTeam(@RequestBody List<String> userIds, @PathVariable String teamId){
        teamService.tryAssignUsersToTeam(teamId, userIds);
    }
}
