package com.opsgenie.workshop.sqs;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import com.opsgenie.workshop.dto.MessageConverter;
import com.opsgenie.workshop.dto.TeamMessage;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;

@Component
public class SqsAccessor {
    private AmazonSQS sqs;
    private String sqsQueueUrl;
    @Value("${aws.access-key}")
    private String accessKey;
    @Value("${aws.secret-key}")
    private String secretKey;

    @PostConstruct
    public void init() {
        AWSCredentials awsCredentials = new BasicAWSCredentials(accessKey, secretKey);
        AWSCredentialsProvider awsCredentialsProvider = new AWSStaticCredentialsProvider(awsCredentials);
        AmazonSQSClientBuilder builder = AmazonSQSClientBuilder.standard();
        builder.setRegion("eu-west-1");
        builder.setCredentials(awsCredentialsProvider);
        sqs = builder.build();
        sqsQueueUrl = sqs.getQueueUrl("team_assign_team_member_queue").getQueueUrl();
    }

    public void sendMessageToQueue(TeamMessage message){
        SendMessageRequest send_msg_request = new SendMessageRequest()
                .withQueueUrl(sqsQueueUrl)
                .withMessageBody(MessageConverter.objectToJson(message));
                sqs.sendMessage(send_msg_request);
    }

    public List<Message> receiveMessages(){
        return sqs.receiveMessage(sqsQueueUrl).getMessages();
    }
    public void deleteMessages(List<Message> messageList){
        for(Message message :  messageList){
            sqs.deleteMessage(sqsQueueUrl,message.getReceiptHandle());
        }
    }
    public AmazonSQS getSqs() {
        return sqs;
    }

    public void setSqs(AmazonSQS sqs) {
        this.sqs = sqs;
    }

    public String getSqsQueueUrl() {
        return sqsQueueUrl;
    }

    public void setSqsQueueUrl(String sqsQueueUrl) {
        this.sqsQueueUrl = sqsQueueUrl;
    }
}
