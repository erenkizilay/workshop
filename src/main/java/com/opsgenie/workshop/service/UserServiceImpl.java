package com.opsgenie.workshop.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import com.opsgenie.workshop.dao.UserDao;
import com.opsgenie.workshop.model.User;

import java.util.HashMap;
import java.util.Map;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    @Qualifier("userDaoDynamoDB")
    private UserDao userDao;



    public UserServiceImpl(@Qualifier("userDaoMemory")UserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    public User getUser(String id) {

        return this.userDao.getUser(id);
    }

    @Override
    public void createUser(User user) {

        this.userDao.createUser(user);
    }

    @Override
    public Map<String, User> getUsers() {
        return this.userDao.getUsers();
    }

    @Override
    public void updateUser(User user) {
        this.userDao.updateUser(user);
    }

    @Override
    public void deleteUser(String id) {
        this.userDao.deleteUser(id);
    }


}
