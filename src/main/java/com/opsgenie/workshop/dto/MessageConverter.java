package com.opsgenie.workshop.dto;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

public class MessageConverter {
    private static ObjectMapper mapper = new ObjectMapper();
    public static String objectToJson(TeamMessage message){
        try {
            return mapper.writeValueAsString(message);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }
    public static TeamMessage jsonToObject(String json){
        try {
            return mapper.readValue(json, TeamMessage.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
