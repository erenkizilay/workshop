package com.opsgenie.workshop.dao;

import org.springframework.stereotype.Repository;
import com.opsgenie.workshop.model.Team;
import com.opsgenie.workshop.model.User;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

@Repository(value = "userDaoMemory")
public class UserDaoInMemoryImpl implements UserDao {

    private HashMap<String, User> users = new HashMap<>();
    private HashMap<String, Team> teams = new HashMap<>();

    @PostConstruct
    void postConstruct() {
        HashMap<String, User> teamMembers = new HashMap<>();
        users.put("1",new User("1","Eren"));
        users.put("2",new User("2","Cem"));
        //teamMembers.put("1",users.get("1"));
        //teamMembers.put("2",users.get("2"));
        //teams.put("team1",new Team("team1","heimdall",teamMembers));
        // TODO user uarat
        // Team yarat
        // user --> team
    }

    @Override
    public User getUser(String id) {
        return users.get(id);
    }

    @Override
    public void createUser(User user) {
        users.put(user.getId(),user);
    }

    @Override
    public Map<String, User> getUsers() {
        return this.users;
    }

    @Override
    public void updateUser(User user) {
        users.put(user.getId(),user);
    }
    @Override
    public void deleteUser(String id) {
        users.remove(id);
    }
}
